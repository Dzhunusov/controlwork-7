import React, {useState} from 'react';
import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
import { faPizzaSlice } from '@fortawesome/free-solid-svg-icons';
import { faHotdog } from '@fortawesome/free-solid-svg-icons';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import MenuItem from "../components/MenuItem/menuItems";
import Order from "../components/OrderPrice/Order";

function App() {
  const [items, setItems] = useState([
    {icon: faHamburger, title: "Hamburger", price: 80},
    {icon: faPizzaSlice, title: "Pizza", price: 90},
    {icon: faHotdog, title: "HotDog", price: 58},
    {icon: faCoffee, title: "Coffee", price: 30},
    {icon: faCoffee, title: "Tea", price: 20},
    {icon: faCoffee, title: "Pepsi", price: 40},
  ])

  const addOrder = () => {
    const orderBlock = document.querySelector('.order-item');
    console.log(orderBlock);
    const orderList = items.map((item, index) => {
      return (
          <Order
              title={items[index].title}
              price={items[index].price}
              count= '1'/>
      )
    })
    orderBlock.innerHTML = orderList;
  }

  let itemList = items.map((item, index) => {
    return (
        <MenuItem
            key={index}
            icon={<FontAwesomeIcon icon={items[index].icon}/>}
            title={items[index].title}
            price={items[index].price}
            addFood={addOrder}/>
    )
  })


  return (
    <div className="App">
      <div className="Order">
        <h2 className="main-title">Order Details</h2>
        <div className="order-item">
          Order is empty!
          Please add some items!
          <Order
              title={items[0].title}
              price={items[0].price}
              count= '1'/>
        </div>
      </div>
      <div className="Menu">
        <h2 className="main-title">Add items</h2>
        <div className='menu-box food'>
          {itemList}
        </div>
      </div>
    </div>
  );
}

export default App;
