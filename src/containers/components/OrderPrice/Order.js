import React from 'react';
import './Order.css'

const Order = props => {
  return (
      <div className="order-info">
        <p>{props.title} <i>x {props.count}</i> {props.price}KGS</p>
        <button>X</button>
      </div>
  );
};

export default Order;