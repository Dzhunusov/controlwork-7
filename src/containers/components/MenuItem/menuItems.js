import React from 'react';
import './MenuItem.css';

const MenuItem = props => {
  return (
      <div className="menu-item" onClick={props.addFood}>
        <p className="item-icon">{props.icon}</p>
        <div className="item-info">
          <h3>{props.title}</h3>
          <p>Price: <b>{props.price}</b> KGS</p>
        </div>
      </div>
  );
};

export default MenuItem;